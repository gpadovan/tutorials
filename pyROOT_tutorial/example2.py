# example2.py

import ROOT

# Write some C++ code in a string

cpp_code = """
// Function definition
int f(int i) {return i*i; }


// Class definition
class A {
public:
A() { cout << "Hello PyROOT!" << endl; }
};
"""

# Inject the code in the ROOT interpreter
ROOT.gInterpreter.ProcessLine(cpp_code)

# We find all the C++ entities in Python, right away!
a = ROOT.A() # prints Hello PyROOT!
x = ROOT.f(3) # x = 9

