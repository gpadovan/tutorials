# tutorials

This repository contains all the tutorials done during the PhD project.

Here follows a list of the tutorials done:

* **ATLAS software tutorial**: introductory toturial for ATLAS newcomers (Nov 2020 + Feb 2021 editions).
* **pyROOT tutorial**: tutorial of ROOT for python, reference link: https://root.cern/manual/python/ .
* **cabinetry tutorial**: tutorial for cabinetry python package for statistical analysis, reference link: https://github.com/cabinetry/cabinetry-tutorials 

----------------------------
Author: G. Padovano, INFN Roma & Sapienza Universita' di Roma