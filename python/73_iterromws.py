# 73_iterrows.py


import pandas as pd

df = pd.DataFrame({'age':[20,32], 'state':['NY','CA'], 'point':[64,92]}, index = ['Alice', 'Bob'])

print(df)

for index, row in df.iterrows():
    print(type(index))
    print(index)
    print("------")
    
    print(type(row))
    print(row)
    print("------")


