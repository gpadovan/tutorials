# 38_exceptions.py

def lunch():
    print("Ho eseguito la funzione lunch()")

try:
    lunch()
    print("Sto dentro al try")
except SyntaxError:
    print("Fix your syntax")
except TypeError:
    print("Oh no! A TypeError has occurred")
except ValueError:
    print("A ValueError occurred!")
except ZeroDivisionError:
    print("Divided by zero?")
else:
    print("No exception")
finally:
    print("This is always printed!")


