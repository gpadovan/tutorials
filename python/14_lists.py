# 14_lists.py

list = ["new York", "Los Angeles", "Boston", "Denver"]

print(list)
print(list[0])

list2 = [1,3,4,5,6,7,8,9]

print(sum(list2))
print(min(list2))
print(max(list2))

print(list2[0])
print(list2[-1])


list3 = ["G", "R", 2, ".."]
print(list3)


print(type(list3[0]))
print(type(list3[2]))
print(list3[2])
