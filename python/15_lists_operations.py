# 15_lists_operations.py

x = [3, 4, 5]

x.append(6)

print(x)

x.append(7)

print(x)

x.pop()
print(x)

x.pop()
print(x)


x = [3, 4, 5]

print(x[0])

print(x[1])

print(x[-1])

print(x[-2])
