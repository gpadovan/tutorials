# 7_join.py

# deifne strings
firstname = "Bugs"
lastname = "Bunny"

# define our sequence
sequence = (firstname, lastname)

# join into the new string
name = " ".join(sequence)
print(name)

name_2 = " ,".join(sequence)
print(name_2)


a = "Giovanni"
b = "Padovano"
c = "nato"
d = "a"
e = "Roma"


seq_2 = (a, b, c, d, e)

name_2 = " - ".join(seq_2)
print(name_2)

words = ["how", "are", "you", "doing", "?"]

sentence = ' '.join(words)
print(sentence)
