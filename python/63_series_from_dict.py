# 63_series_from_dict.py

import pandas as pd
import numpy as np

data = { 'uk':'united kingdom', 'fr':'france' }
s = pd.Series(data)

print(data)
print(s)


data2 = {'a':1, 'b':2, 'c':3, 'd':4}
s2 = pd.Series(data2)
print(s2)

data3 = {'a':1.2, 'b':4.5, 'c':3.467}
s3 = pd.Series(data3)

print(data3)
print(s3)

data4 = {'a':'a1', 'b':'b2', 'c':'prova a casp'}
s4 = pd.Series(data4)

print(s4)
print(data4)
