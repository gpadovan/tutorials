# 66_select_column.pyll

import pandas as pd

data = [['Axel', 32], ['Alice', 26], ['Alex', 45]]

df = pd.DataFrame(data, columns=['Name', 'Age'])

print(df)

col = df['Name']
print(col)
