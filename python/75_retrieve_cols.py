# 75_retrieve_cols.py

import pandas as pd

df = pd.DataFrame({'age': [20, 32], 'state': ['NY', 'CA'], 'point': [64, 92]}, index=['Alice', 'Bob'])

print(df)


print(df['age'])

print(df['state'])

print(df['point'])

print(type(df['age']))

for age in df['age']:
    print(age)

# multiple values together
for age, point in zip(df['age'], df['point']):
    print(age, point)


for name, state, point in zip(df['age'], df['state'], df['point']):
    print(age, state, point)



# use the index attribute
print(df.index)

print(type(df.index))

for index in df.index:
    print(index)

for index, state in zip(df.index, df['state']):
    print(index, state)


