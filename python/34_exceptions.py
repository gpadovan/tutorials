# 34_exceptions.py

try: 
    x = input("Enter number: ")
    x = x + 1
    print(x)
except:
    print("Invalid input")
else:
    print("Valid input")
finally:
    print("To be printed in any case")
