# 25_return.py

def complexfunction(a,b):
    sum = a+b
    return sum


s = complexfunction(2,3)
print(s)

def getPerson():
    name = "Loena"
    age = 35
    country = "UK"
    
    return name, age, country

name, age, country = getPerson()
print(name)
print(age)
print(country)


