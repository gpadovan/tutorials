# 61_series_from_list.py

import pandas as pd

items = [1,2,3,4]
s = pd.Series(items)

print(items)
print(s)


items_2 = ["P", "R", "c"]
s2 = pd.Series(items_2)

print(items_2)
print(s2)


items3 = [1.1, 0.45, 6.567]
s3 = pd.Series(items3)
print(items3)
print(s3)
