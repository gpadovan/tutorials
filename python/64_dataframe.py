# 64_dataframe.py

import pandas as pd

data = [1,2,3]

df = pd.DataFrame(data)

print("data frame")
print(df)

s = pd.Series(data)

print("series")
print(s)
