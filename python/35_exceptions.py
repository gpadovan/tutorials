# 35_exceptions.py

def fail():
    1/0

try:
    fail()
except:
    print("Exeption occurred")

print("Program continues")
