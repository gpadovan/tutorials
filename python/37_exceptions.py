# 37_exceptions.py

try:
    x = 1
except:
    print("Failed to set x")
else:
    print("No exception occurred")
finally:
    print("We always do this")

