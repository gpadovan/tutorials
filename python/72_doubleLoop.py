# 72_doubleLoop.py

import pandas as pd

df = pd.DataFrame({'age':[20,32], 'state':['NY', 'CA'], 'point':[64,92]}, index=['Alice', 'Bob'])

print(df)

for column_name, item in df.iteritems():
    print(type(column_name))
    print(column_name)
    print(type(item))
    print(item)

    print("============")
