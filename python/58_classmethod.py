# 58_classMethod.py


class Fruit:
    name = 'Fruitas'

    @classmethod
    def printName(cls):
        print("the name is: ", cls.name)

Fruit.printName()

apple = Fruit()
berry = Fruit()

Fruit.printName()
apple.printName()
berry.printName()

apple.name = "apple"
Fruit.printName()
apple.printName()
berry.printName()


Fruit.name = "Apple"
Fruit.printName()
apple.printName()
berry.printName()
