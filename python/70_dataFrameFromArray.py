# 70_dataFrameFromArray.py

import numpy as np
import pandas as pd


ar = np.array([[1,2,3],[4,5,6],[6,7,8]])

print(ar)

df = pd.DataFrame(ar)
print(df)


df2 = pd.DataFrame(ar, index=['A', 'B', 'C'], columns=['One', 'Two', 'Three'])

print(df2)

# copy a dataframe
print("---------------------------------------")
df22 = df2[['One', 'Two']].copy()
print(df22)
print("---------------------------------------")
df3 = df2[ ['One'] ].copy()
print(df3)
print("---------------------------------------")
df4 = df2[ 'One' ].copy()
print(df4)
