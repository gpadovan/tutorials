# 27_global.py

balance = 0

def addAmount(x, balance):
    balance = balance + x
    return balance

balance = addAmount(5, balance)
print(balance)
