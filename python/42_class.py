class dog:
    def __init__(self,name,age):
        self.name = name
        self.age = age

    def printDog(self):
        print("name: {0}".format(self.name))
        print("age: {0}".format(self.age))
    
    def bark(self):
        print("bark!")



dog1 = dog("Nerone", 30)
dog2 = dog("Colly", 16)


dog1.printDog()
dog2.printDog()

dog1.bark()
dog2.bark()

dog3 = dog("Pippo", "789")

dog3.printDog()
