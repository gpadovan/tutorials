# 50_inheritance.py

class App:
    def start(self):
        print('starting')

class Android(App):
    def getVersion(self):
        print('Android version')

app = App()
app.start()

android_app = Android()
android_app.start()
android_app.getVersion()


