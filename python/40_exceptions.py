# 40_exceptions.py

class NoMoneyExceptions(Exception):
    pass

class OutOfBudget(Exception):
    pass


balance = int(input("Enter a balance: "))
if balance < 1000:
    raise NoMoneyException
elif balance > 100000:
    raise OutOfBudget

