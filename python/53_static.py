# 53_static.py

class Music:
    @staticmethod
    def play():
        print("*playing music*")
    
    @staticmethod
    def stop():
        print("stop playing")


Music.play()
Music.stop()

obj=Music()
obj.stop()

obj.play()
