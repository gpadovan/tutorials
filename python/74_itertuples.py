# 74_itertuples.py

import pandas as pd

df = pd.DataFrame({'age':[20,32], 'state':['NY','CA'],'point':[64,92]}, index=['Alice', 'Bob'])

for row in df.itertuples():
    print(type(row))
    print(row)
    print('---------')

    print(row[3])
    print(row.point)
    print('----------\n')


