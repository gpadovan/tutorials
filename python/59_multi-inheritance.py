# 59_multi-inheritance

class Human:
    name = ""

class Coder: 
    skills = 3

class Pythonista(Human, Coder):
    version = 4

obj = Pythonista()
obj.name = "Alice"

print(obj.name)
print(obj.version)
print(obj.skills)


