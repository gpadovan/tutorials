# 67_column_addition.py

import pandas as pd

data = [ ['Axel', 32], ['Alice', 26], ['Alex', 45] ]
df = pd.DataFrame(data, columns=['Name', 'Age'])

c = pd.DataFrame([1,2,3], columns=['Example'])

print(df)
print(c)


# now I add the column

df['Example'] = c['Example']

print(df)

df['Custom col'] = c['Example']

print(df)

df['Custom col 2'] = df['Name']

print(df)

del df['Custom col 2']

print(df)

del df['Custom col']

print(df)

del df['Example']

print(df)
