# 68_select_row.py

import pandas as pd

data = [['Axel', 32], ['Alice', 26], ['Alex', 45]]

df = pd.DataFrame(data, columns = ['Name', 'Age'])

print(df)

r0 = df.loc[0]
r2 = df.loc[2]

print(r0)
print(r2)

# now we append a row
print('===================================')
user = pd.DataFrame([['Vivian',33]], columns = ['Name', 'Age'])

df = df.append(user)

print(df)

user23 = pd.DataFrame([['D', 32], ['G', 30], ['R', 28]], columns=['Name', 'Age'])
df = df.append(user23)

print(df)

# now we delete a row
print('===================================')

df = df.drop(0)
print(df)


