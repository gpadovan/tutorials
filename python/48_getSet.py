# 48_getSet.py

class Friend:
    def __init__(self):
        self.job = "None"

    def getJob(self):
        return self.job
        
    def setJob(self, job):
        self.job = job

Alice = Friend()
Bob = Friend()

Alice.setJob("Carpenter")
Bob.setJob("Builder")


print(Bob.job)
print(Alice.job)

R = Friend()
R.setJob("Physicist")

r_job = R.job

print("R is a beautifoul {0}".format(r_job))
