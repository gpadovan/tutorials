# 9_random.py

import random

# Crate a random floating point and print it
print(random.random())

# random btw (0, 10)
print(random.randrange(0, 10))

# random floating btw 0 and 10
print(random.uniform(0,10))

# random float between 0 and 10
x = random.uniform(1, 10)
print(x)


