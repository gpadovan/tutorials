# make_dataset.py
# generates a fake dataset to be fitted in cabinetry.
#
# Author: G. Padovano - CERN
# mailto: giovanni.padovano@cern.ch

import ROOT

ofile = ROOT.TFile("./inputs/histograms.root", "recreate")

sys_list = ["sys1", "sys2"]

### Signal_region

# nominal
h_data = ROOT.TH1F("Data_Nominal", "", 5, 0, 5)
h_data.SetBinContent(1, 100.)
h_data.SetBinContent(2, 120.)
h_data.SetBinContent(3, 115.)
h_data.SetBinContent(4, 92.) # ~ no signal: 92. | 100% signal: 105. 
h_data.SetBinContent(5, 75.) #              75. |              100.

h_sgn = ROOT.TH1F("Signal_Nominal", "", 5, 0, 5)
h_sgn.SetBinContent(1, 0.)
h_sgn.SetBinContent(2, 0.)
h_sgn.SetBinContent(3, 5.)
h_sgn.SetBinContent(4, 15.)
h_sgn.SetBinContent(5, 30.)

h_bkg_1 = ROOT.TH1F("Background_1_Nominal", "", 5, 0, 5)
h_bkg_1.SetBinContent(1, 90.)
h_bkg_1.SetBinContent(2, 100.)
h_bkg_1.SetBinContent(3, 80.)
h_bkg_1.SetBinContent(4, 50.)
h_bkg_1.SetBinContent(5, 20.)

h_bkg_2 = ROOT.TH1F("Background_2_Nominal", "", 5, 0, 5)
h_bkg_2.SetBinContent(1, 10.)
h_bkg_2.SetBinContent(2, 20.)
h_bkg_2.SetBinContent(3, 30.)
h_bkg_2.SetBinContent(4, 40.)
h_bkg_2.SetBinContent(5, 50.)


# systematics
h_bkg_1_sys = dict()
for sys in sys_list:
    h_bkg_1_sys[sys+"_1up"] = h_bkg_1.Clone("Background_1_"+sys+"_1up")
    h_bkg_1_sys[sys+"_1up"].Scale(1.1)
    
    h_bkg_1_sys[sys+"_1down"] = h_bkg_1.Clone("Background_1_"+sys+"_1down")
    h_bkg_1_sys[sys+"_1down"].Scale(0.9)

h_bkg_2_sys = dict()
for sys in sys_list:
    h_bkg_2_sys[sys+"_1up"] = h_bkg_1.Clone("Background_2_"+sys+"_1up")
    h_bkg_2_sys[sys+"_1up"].Scale(1.2)
    
    h_bkg_2_sys[sys+"_1down"] = h_bkg_1.Clone("Background_2_"+sys+"_1down")
    h_bkg_2_sys[sys+"_1down"].Scale(0.8)
    
    
# write histograms
sr_dir = ofile.mkdir("SR")
sr_dir.cd()
h_data.Write()
h_sgn.Write()
h_bkg_1.Write()
h_bkg_2.Write()

for sys in sys_list:
    h_bkg_1_sys[sys+"_1up"].Write()
    h_bkg_1_sys[sys+"_1down"].Write()
    h_bkg_2_sys[sys+"_1up"].Write()
    h_bkg_2_sys[sys+"_1down"].Write()


### Control_region_1

# nominal
h_data.Reset()
h_data.SetBinContent(1, 81.)
h_data.SetBinContent(2, 95.)
h_data.SetBinContent(3, 87.)
h_data.SetBinContent(4, 75.)
h_data.SetBinContent(5, 61.)

h_sgn.Reset()
h_sgn.SetBinContent(1, 0.)
h_sgn.SetBinContent(2, 0.)
h_sgn.SetBinContent(3, 0.)
h_sgn.SetBinContent(4, 0.)
h_sgn.SetBinContent(5, 0.)

h_bkg_1.Reset()
h_bkg_1.SetBinContent(1, 80.)
h_bkg_1.SetBinContent(2, 90.)
h_bkg_1.SetBinContent(3, 84.)
h_bkg_1.SetBinContent(4, 73.)
h_bkg_1.SetBinContent(5, 61.)

h_bkg_2.Reset()
h_bkg_2.SetBinContent(1, 1.)
h_bkg_2.SetBinContent(2, 5.)
h_bkg_2.SetBinContent(3, 3.)
h_bkg_2.SetBinContent(4, 2.)
h_bkg_2.SetBinContent(5, 0.)


# systematics
for sys in sys_list:
    h_bkg_1_sys[sys+"_1up"] = h_bkg_1.Clone("Background_1_"+sys+"_1up")
    h_bkg_1_sys[sys+"_1up"].Scale(1.1)
    
    h_bkg_1_sys[sys+"_1down"] = h_bkg_1.Clone("Background_1_"+sys+"_1down")
    h_bkg_1_sys[sys+"_1down"].Scale(0.9)

for sys in sys_list:
    h_bkg_2_sys[sys+"_1up"] = h_bkg_1.Clone("Background_2_"+sys+"_1up")
    h_bkg_2_sys[sys+"_1up"].Scale(1.2)
    
    h_bkg_2_sys[sys+"_1down"] = h_bkg_1.Clone("Background_2_"+sys+"_1down")
    h_bkg_2_sys[sys+"_1down"].Scale(0.8)


# write histograms
cr1_dir = ofile.mkdir("CR1")
cr1_dir.cd()
h_data.Write()
h_sgn.Write()
h_bkg_1.Write()
h_bkg_2.Write()

for sys in sys_list:
    h_bkg_1_sys[sys+"_1up"].Write()
    h_bkg_1_sys[sys+"_1down"].Write()
    h_bkg_2_sys[sys+"_1up"].Write()
    h_bkg_2_sys[sys+"_1down"].Write()



### Control_region_2

# nominal
h_data.Reset()
h_data.SetBinContent(1, 81.)
h_data.SetBinContent(2, 95.)
h_data.SetBinContent(3, 87.)
h_data.SetBinContent(4, 75.)
h_data.SetBinContent(5, 61.)

h_sgn.Reset()
h_sgn.SetBinContent(1, 0.)
h_sgn.SetBinContent(2, 0.)
h_sgn.SetBinContent(3, 0.)
h_sgn.SetBinContent(4, 0.)
h_sgn.SetBinContent(5, 0.)

h_bkg_1.Reset()
h_bkg_1.SetBinContent(1, 1.)
h_bkg_1.SetBinContent(2, 5.)
h_bkg_1.SetBinContent(3, 3.)
h_bkg_1.SetBinContent(4, 2.)
h_bkg_1.SetBinContent(5, 0.)

h_bkg_2.Reset()
h_bkg_2.SetBinContent(1, 80.)
h_bkg_2.SetBinContent(2, 90.)
h_bkg_2.SetBinContent(3, 84.)
h_bkg_2.SetBinContent(4, 73.)
h_bkg_2.SetBinContent(5, 61.)


# systematics
for sys in sys_list:
    h_bkg_1_sys[sys+"_1up"] = h_bkg_1.Clone("Background_1_"+sys+"_1up")
    h_bkg_1_sys[sys+"_1up"].Scale(1.1)
    
    h_bkg_1_sys[sys+"_1down"] = h_bkg_1.Clone("Background_1_"+sys+"_1down")
    h_bkg_1_sys[sys+"_1down"].Scale(0.9)

for sys in sys_list:
    h_bkg_2_sys[sys+"_1up"] = h_bkg_1.Clone("Background_2_"+sys+"_1up")
    h_bkg_2_sys[sys+"_1up"].Scale(1.2)
    
    h_bkg_2_sys[sys+"_1down"] = h_bkg_1.Clone("Background_2_"+sys+"_1down")
    h_bkg_2_sys[sys+"_1down"].Scale(0.8)


# write histograms
cr2_dir = ofile.mkdir("CR2")
cr2_dir.cd()
h_data.Write()
h_sgn.Write()
h_bkg_1.Write()
h_bkg_2.Write()

for sys in sys_list:
    h_bkg_1_sys[sys+"_1up"].Write()
    h_bkg_1_sys[sys+"_1down"].Write()
    h_bkg_2_sys[sys+"_1up"].Write()
    h_bkg_2_sys[sys+"_1down"].Write()
