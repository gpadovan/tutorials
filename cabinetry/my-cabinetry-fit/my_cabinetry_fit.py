# my_cabinetry_fit.py
# Fit built by me, to test cabinetry: 1 SR, 2 CR, some sys.
#
# Author: G. Padovano - CERN
# mailto: giovanni.padovano@cern.ch

import cabinetry

# set verbose option on cabinetry
cabinetry.set_logging()

### 1. Load the configuration file
cabinetry_config = cabinetry.configuration.load("config.yml")
cabinetry.configuration.print_overview(cabinetry_config)

cabinetry.templates.collect(cabinetry_config, method="uproot")
cabinetry.templates.postprocess(cabinetry_config)

### 2. Workspace building
workspace_path = "workspace/my_workspace.json"
ws = cabinetry.workspace.build(cabinetry_config)
cabinetry.workspace.save(ws, workspace_path)

### 3. Model structure
ws = cabinetry.workspace.load(workspace_path)
model, data = cabinetry.model_utils.model_and_data(ws, asimov=False)

### 4. Fitting
fit_results = cabinetry.fit.fit(model, data)

### 5. Limit extraction
limit_results = cabinetry.fit.limit(model, data)
#cabinetry.visualize.limit(limit_results) # segmentation fault

### 6. Discovery significance
significance_results = cabinetry.fit.significance(model, data)


### It works!
