# setup_cabinetry.sh
#
# sets up cabinetry package and the proper versions or python3 and ROOT.
# Author: G. Padovano - CERN
# mailto: giovanni.padovano@cern.ch


setupATLAS
lsetup "python 3.9.13-x86_64-centos7"
lsetup "root 6.26.08-x86_64-centos7-gcc11-opt"
python3 -m pip install cabinetry[contrib]

