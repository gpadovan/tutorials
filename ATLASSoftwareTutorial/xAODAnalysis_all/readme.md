# xAODAnalysis_all


This folder contains the last iteration of the xAOD tutorial, as included in the ATLAS Software Tutorial (edition February 2021).

Reference link: https://atlassoftwaredocs.web.cern.ch/ABtutorial/

--------------------------------
Author: G. Padovano, INFN Roma & Sapienza Universita' di Roma