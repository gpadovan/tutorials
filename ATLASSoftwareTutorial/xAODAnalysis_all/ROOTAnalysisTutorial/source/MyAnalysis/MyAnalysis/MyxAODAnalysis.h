#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <TH1.h>
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h> //GRL
#include <AsgTools/ToolHandle.h>
#include <TTree.h>
#include <vector>

class MyxAODAnalysis : public EL::AnaAlgorithm
{
 public:
  // this is a standard algorithm constructor
  MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
  ~MyxAODAnalysis () override; // se decommentato da errore ???

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  ToolHandle<IGoodRunsListSelectionTool> m_grl;



 private:
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;

  /// Electron pT cut
  double m_electronPtCut;
  /// Sample name
  std::string m_sampleName;

  /// output variables for the current event
  /// \{
  unsigned int m_runNumber = 0; ///< Run number
  unsigned long long m_eventNumber = 0; ///< Event number
  /// Jet 4-momentum variables
  std::vector<float> *m_jetEta = nullptr;
  std::vector<float> *m_jetPhi = nullptr;
  std::vector<float> *m_jetPt = nullptr;
  std::vector<float> *m_jetE = nullptr;
  /// \}


};

#endif
